// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once 

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "unreal_git_hw3HUD.generated.h"

UCLASS()
class Aunreal_git_hw3HUD : public AHUD
{
	GENERATED_BODY()

public:
	Aunreal_git_hw3HUD();

	/** Primary draw call for the HUD */
	virtual void DrawHUD() override;

private:
	/** Crosshair asset pointer */
	class UTexture2D* CrosshairTex;

};

