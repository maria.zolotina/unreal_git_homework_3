// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "unreal_git_hw3GameMode.h"
#include "unreal_git_hw3HUD.h"
#include "unreal_git_hw3Character.h"
#include "UObject/ConstructorHelpers.h"

Aunreal_git_hw3GameMode::Aunreal_git_hw3GameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = Aunreal_git_hw3HUD::StaticClass();
}
