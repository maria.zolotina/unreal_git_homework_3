// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "unreal_git_hw3.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, unreal_git_hw3, "unreal_git_hw3" );
 