// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "unreal_git_hw3GameMode.generated.h"

UCLASS(minimalapi)
class Aunreal_git_hw3GameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	Aunreal_git_hw3GameMode();
};



